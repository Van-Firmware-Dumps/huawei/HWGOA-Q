#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_ohosqssi.mk

COMMON_LUNCH_CHOICES := \
    lineage_ohosqssi-user \
    lineage_ohosqssi-userdebug \
    lineage_ohosqssi-eng

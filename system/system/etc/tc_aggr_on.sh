#!/bin/bash
#
# This file is added for packet aggration feature
# Copyright © Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
#

system/bin/sh -c  "tc qdisc add dev rmnet_data2 root handle 1:0 aggrq aggrlen 1kb on 1ms off 200ms"

exit 0

#!/bin/bash
#
# This file is added for packet aggration feature
# Copyright © Huawei Technologies Co., Ltd. 2023-2023. All rights reserved.
#

system/bin/sh -c  "tc qdisc del dev rmnet_data2 root"

exit 0

<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (C) 2015 The Android Open Source Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->
<!-- Volume section defines a volume curve for a given use case and device category.
It contains a list of points of this curve expressing the attenuation in Millibels for a given
volume index from 0 to 100.
<volume stream=”AUDIO_STREAM_MUSIC” deviceCategory=””>
<point>0,-9600</point>
<point>100,0</point>
</volume>
-->

<volumes>
    <volume stream="AUDIO_STREAM_VOICE_CALL" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                             ref="DEFAULT_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_VOICE_CALL" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                             ref="SPEAKER_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_VOICE_CALL" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                             ref="SPEAKER_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_VOICE_CALL" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                             ref="DEFAULT_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_VOICE_CALL" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                             ref="DEFAULT_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_VOICE_CALL" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                             ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_SYSTEM" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                         ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_SYSTEM" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                         ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_SYSTEM" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                         ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_SYSTEM" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                         ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_SYSTEM" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                         ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_SYSTEM" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                         ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_RING" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                       ref="DEFAULT_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_RING" deviceCategory="DEVICE_CATEGORY_SPEAKER">
        <point>1,-6000</point>
        <point>6,-4100</point>
        <point>13,-3400</point>
        <point>20,-2900</point>
        <point>26,-2400</point>
        <point>33,-2050</point>
        <point>40,-1750</point>
        <point>46,-1500</point>
        <point>53,-1300</point>
        <point>60,-1100</point>
        <point>66,-900</point>
        <point>73,-750</point>
        <point>80,-650</point>
        <point>86,-500</point>
        <point>93,-300</point>
        <point>100,0</point>
    </volume>
    <volume stream="AUDIO_STREAM_RING" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                       ref="DEFAULT_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_RING" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                       ref="DEFAULT_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_RING" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                       ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_RING" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                       ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_MUSIC" deviceCategory="DEVICE_CATEGORY_HEADSET">
        <point>1,-5900</point>
        <point>6,-5750</point>
        <point>13,-5250</point>
        <point>20,-4750</point>
        <point>26,-4250</point>
        <point>33,-3750</point>
        <point>40,-3350</point>
        <point>46,-2950</point>
        <point>53,-2550</point>
        <point>60,-2150</point>
        <point>66,-1750</point>
        <point>73,-1400</point>
        <point>80,-1050</point>
        <point>86,-700</point>
        <point>93,-350</point>
        <point>100,0</point>
        </volume>
    <volume stream="AUDIO_STREAM_MUSIC" deviceCategory="DEVICE_CATEGORY_SPEAKER">
        <point>1,-8000</point>
        <point>6,-5200</point>
        <point>13,-4500</point>
        <point>20,-3800</point>
        <point>26,-3200</point>
        <point>33,-2700</point>
        <point>40,-2300</point>
        <point>46,-1900</point>
        <point>53,-1600</point>
        <point>60,-1350</point>
        <point>66,-1100</point>
        <point>73,-900</point>
        <point>80,-700</point>
        <point>86,-450</point>
        <point>93,-250</point>
        <point>100,0</point>
    </volume>
    <volume stream="AUDIO_STREAM_MUSIC" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                        ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_MUSIC" deviceCategory="DEVICE_CATEGORY_BLUETOOTH">
        <point>1,-6800</point>
        <point>6,-6000</point>
        <point>13,-5400</point>
        <point>20,-4900</point>
        <point>26,-4400</point>
        <point>33,-3900</point>
        <point>40,-3400</point>
        <point>46,-2900</point>
        <point>53,-2500</point>
        <point>60,-2100</point>
        <point>66,-1700</point>
        <point>73,-1300</point>
        <point>80,-1000</point>
        <point>86,-700</point>
        <point>93,-450</point>
        <point>100,-300</point>
    </volume>
    <volume stream="AUDIO_STREAM_MUSIC" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                        ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_MUSIC" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                        ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ALARM" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                        ref="DEFAULT_NON_MUTABLE_HEADSET_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ALARM" deviceCategory="DEVICE_CATEGORY_SPEAKER">
        <point>0,-5000</point>
        <point>6,-4100</point>
        <point>13,-3400</point>
        <point>20,-2900</point>
        <point>26,-2400</point>
        <point>33,-2050</point>
        <point>40,-1750</point>
        <point>46,-1500</point>
        <point>53,-1300</point>
        <point>60,-1100</point>
        <point>66,-900</point>
        <point>73,-750</point>
        <point>80,-650</point>
        <point>86,-500</point>
        <point>93,-300</point>
        <point>100,0</point>
    </volume>
    <volume stream="AUDIO_STREAM_ALARM" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                        ref="DEFAULT_NON_MUTABLE_EARPIECE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ALARM" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                        ref="DEFAULT_NON_MUTABLE_HEADSET_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ALARM" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                        ref="DEFAULT_NON_MUTABLE_EXT_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ALARM" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                        ref="DEFAULT_NON_MUTABLE_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_NOTIFICATION" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                               ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_NOTIFICATION" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                               ref="SPEAKER_NOTIFICATION_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_NOTIFICATION" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                               ref="DEFAULT_DEVICE_CATEGORY_EARPIECE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_NOTIFICATION" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                               ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_NOTIFICATION" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                               ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_NOTIFICATION" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                               ref="DEFAULT_DEVICE_CATEGORY_HEADSET_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_BLUETOOTH_SCO" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                                ref="DEFAULT_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_BLUETOOTH_SCO" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                                ref="SPEAKER_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_BLUETOOTH_SCO" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                                ref="DEFAULT_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_BLUETOOTH_SCO" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                                ref="DEFAULT_VOICE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_BLUETOOTH_SCO" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                                ref="DEFAULT_MEDIA_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_BLUETOOTH_SCO" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                                ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ENFORCED_AUDIBLE" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                                   ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ENFORCED_AUDIBLE" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                                   ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ENFORCED_AUDIBLE" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                                   ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ENFORCED_AUDIBLE" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                                   ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ENFORCED_AUDIBLE" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                                   ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ENFORCED_AUDIBLE" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                                   ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_DTMF" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                       ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_DTMF" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                       ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_DTMF" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                       ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_DTMF" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                       ref="DEFAULT_SYSTEM_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_DTMF" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                       ref="DEFAULT_DEVICE_CATEGORY_EXT_MEDIA_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_DTMF" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                       ref="DEFAULT_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_TTS" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                      ref="TTS_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_TTS" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                      ref="TTS_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_TTS" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                      ref="TTS_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_TTS" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                      ref="TTS_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_TTS" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                      ref="TTS_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_TTS" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                      ref="TTS_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ACCESSIBILITY" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                                ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ACCESSIBILITY" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                                ref="DEFAULT_NON_MUTABLE_SPEAKER_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ACCESSIBILITY" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                                ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ACCESSIBILITY" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                                ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ACCESSIBILITY" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                                ref="DEFAULT_NON_MUTABLE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_ACCESSIBILITY" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                                ref="DEFAULT_NON_MUTABLE_HEARING_AID_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_REROUTING" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                            ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_REROUTING" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                            ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_REROUTING" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                            ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_REROUTING" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                            ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_REROUTING" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                            ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_REROUTING" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                            ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_PATCH" deviceCategory="DEVICE_CATEGORY_HEADSET"
                                        ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_PATCH" deviceCategory="DEVICE_CATEGORY_SPEAKER"
                                        ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_PATCH" deviceCategory="DEVICE_CATEGORY_EARPIECE"
                                        ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_PATCH" deviceCategory="DEVICE_CATEGORY_BLUETOOTH"
                                        ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_PATCH" deviceCategory="DEVICE_CATEGORY_EXT_MEDIA"
                                        ref="FULL_SCALE_VOLUME_CURVE"/>
    <volume stream="AUDIO_STREAM_PATCH" deviceCategory="DEVICE_CATEGORY_HEARING_AID"
                                        ref="FULL_SCALE_VOLUME_CURVE"/> 
</volumes>

